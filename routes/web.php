<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/oauth/redirect', [App\Http\Controllers\OauthController::class, 'redirect'])->name('oauth.redirect');
Route::get('/oauth/callback', [App\Http\Controllers\OauthController::class, 'callback'])->name('oauth.callback');
Route::get('/oauth/refresh', [App\Http\Controllers\OauthController::class, 'refresh'])->name('oauth.refresh');
