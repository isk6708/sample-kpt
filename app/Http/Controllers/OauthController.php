<?php

namespace App\Http\Controllers;

use App\Models\OauthToken;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

class OauthController extends Controller
{
    public function redirect(){
        $query = http_build_query([
            'client_id' => '10',
            // 'client_id' => '3',
            'redirect_uri' => 'http://sample-kpt.test:8081/oauth/callback',
            'response_type' => 'code'
        ]);
        
        return redirect('http://10.8.154.17/usim-api-server/public/oauth/authorize?'.$query);
        // return redirect('http://usim-api-server.test:8081/oauth/authorize?'.$query);
    }

    public function callback(Request $req){
        
        $response = Http::asForm()->post('http://10.8.154.17/usim-api-server/public/oauth/token', [
        // $response = Http::asForm()->post('http://usim-api-server.test:8081/oauth/token', [
            'grant_type' => 'authorization_code',
            'client_id' => '10',
            // 'client_id' => '3',
            'client_secret' => '08fLng6UNuyS4ZwQd4m7p3XA13C4wH0bkKvWOYy0',
            // 'client_secret' => 'REHNy0u1TMjLvuRsyZfvReqvN74DWGaTl1tGMm6f',
            
            'redirect_uri' => 'http://sample-kpt.test:8081/oauth/callback',
            'code' => $req->code,
        ]);
    
        $response = $response->json();

        OauthToken::where('user_id',$req->user()->id)->delete();

        $ot = new OauthToken();
        $ot->user_id = $req->user()->id;
        $ot->access_token = $response['access_token'];
        $ot->expires_in = $response['expires_in'];
        $ot->refresh_token = $response['refresh_token'];
        $ot->save();

        return redirect('/home');

        // return $response->json();

    }

    public function refresh(Request $req){

        $token = OauthToken::where('user_id',$req->user()->id)->first();

        $response = Http::asForm()->post('http://10.8.154.17/usim-api-server/public/oauth/token', [
            'grant_type' => 'refresh_token',
            'refresh_token' => $token->refresh_token,
            'client_id' => env('client_id'),
            'client_secret' => env('client_secret'),
            'scope' => '',
        ]);
         
        if ($response->status() === 200) {

            $response = $response->json();
            
            $token->access_token = $response['access_token'];
            $token->expires_in = $response['expires_in'];
            $token->refresh_token = $response['refresh_token'];
            $token->save();
            return redirect('/home');
        }else{
            $token->delete() ;
            return redirect('/home')->withStatus('Unathorized refresh');
        }
    }

}

