<?php

namespace App\Http\Controllers;

use App\Models\OauthToken;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $staffs = [];
        // $ct = auth()->user()->update_at;
        // $cte = $ct+auth()->user()->token->expires_in;
        // dd($ct+auth()->user()->token->expires_in);
        // dd(date('d-m-Y',$cte));
        // dd(date('d-m-Y',auth()->user()->token->expires_in));
        if (Auth::user()->token){

            $token = OauthToken::where('user_id',Auth::user()->id)->first();

            if (now()->gte($token->updated_at->addSeconds($token->expires_in))){
                return redirect('/oauth/refresh');
            }

            $response = Http::withHeaders([
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . auth()->user()->token->access_token
            ])->get('http://10.8.154.17/usim-api-server/public/api/hr/personnel/maklumat-staff');
            // ])->get('http://usim-api-server.test:8081/api/hr/personnel/maklumat-staff');
            
        
            
                // dd(auth()->user()->token->access_token.$response->status());
            if ($response->status() === 200) {
                $staffs = $response->json();
            }
        }
        return view('home',compact('staffs'));
    }
}
