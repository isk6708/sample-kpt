@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    
                    @if(!auth()->user()->token)
                    <a href="/oauth/redirect">Authorize Access To USIM API for Staff List</a>
                    @else
                    <table class='table'>
                        <tr>
                            <th>No Staff</th>
                            <th>Alamat Staff</th>
                        </tr>
                        @foreach($staffs as $staff)
                            <tr>
                                <td>{{$staff['staff_no']}}</td>
                                <td>{{$staff['permanent_addr']}}</td>
                            </tr>
                        @endforeach
                    </table>
                    @endif


                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
